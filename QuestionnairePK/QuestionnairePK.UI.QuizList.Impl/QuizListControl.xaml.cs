﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace QuestionnairePK.UI.QuizList.Impl
{
    /// <summary>
    /// Interaction logic for QuziListControl.xaml
    /// </summary>
    public partial class QuziListControl : UserControl
    {
        public static readonly RoutedEvent DoubleClickEvent =
    EventManager.RegisterRoutedEvent("MouseDoubleClick", RoutingStrategy.Bubble,
    typeof(RoutedEventHandler), typeof(QuziListControl));

        public static readonly RoutedEvent RemoveClickEvent =
    EventManager.RegisterRoutedEvent("MouseUp", RoutingStrategy.Bubble,
    typeof(RoutedEventHandler), typeof(QuziListControl));

        public event RoutedEventHandler DoubleClick
        {
            add { AddHandler(DoubleClickEvent, value); }
            remove { RemoveHandler(DoubleClickEvent, value); }
        }

        public event RoutedEventHandler RemoveClick
        {
            add { AddHandler(RemoveClickEvent, value); }
            remove { RemoveHandler(RemoveClickEvent, value); }
        }


        public QuziListControl()
        {
            InitializeComponent();
        }

        private void Image_MouseUp(object sender, MouseButtonEventArgs e)
        {   

        }

        private void removeButton_MouseUp(object sender, MouseButtonEventArgs e)
        {
            RaiseEvent(new RoutedEventArgs(RemoveClickEvent));
        }

        private void QuizList_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            RaiseEvent(new RoutedEventArgs(DoubleClickEvent));
        }
    }
}
