﻿using QuestionnairePK.Loads.Contract;
using QuestionnairePK.Model;
using QuestionnairePK.Savings.Contract;
using QuestionnairePK.UI.QuizList.Contract;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace QuestionnairePK.UI.QuizList.Impl
{
    public class QuizList : IQuizList
    {
        public event EventHandler DoubleClickQuiz;
        public event EventHandler LoadQuizListCompleted;
        public event EventHandler RemoveQuizComplete;

        private QuziListControl quizListControl;
        private ObservableCollection<Quiz> quizCollection;

        private readonly ILoadsJson loadJson;
        private readonly ISaveJson saveJson;

        private string path;

        public QuizList(ILoadsJson load, ISaveJson save)
        {
            this.loadJson = load;
            this.saveJson = save;

            this.quizListControl = new QuziListControl();
            this.quizCollection = new ObservableCollection<Quiz>();

            InitializeBindings();

            this.quizListControl.DoubleClick += quizListControl_DoubleClick;
            this.quizListControl.RemoveClick += quizListControl_RemoveClick;
        }

        private void InitializeBindings()
        {
            this.quizListControl.QuizList.ItemsSource = quizCollection;
        }


        void quizListControl_RemoveClick(object sender, System.Windows.RoutedEventArgs e)
        {
            var item = sender as QuziListControl;
            var quiz = item.QuizList.SelectedItem as Quiz;

            quizCollection.Remove(quiz);

            if (RemoveQuizComplete != null)
            {
                RemoveQuizComplete(quiz, null);
            }

            //SaveListTaskAsync();
        }

        //private void SaveListTaskAsync()
        //{
        //    if (!String.IsNullOrEmpty(path))
        //    {
        //        Task saveListTask = new Task(() => saveJson.SaveQuizList(quizCollection, path));
        //        saveListTask.Start();
        //    }
        //}

        void quizListControl_DoubleClick(object sender, System.Windows.RoutedEventArgs e)
        {
            var item = sender as QuziListControl;
            var quiz = item.QuizList.SelectedItem as Quiz;

            if (item != null && DoubleClickQuiz != null)
            {
                DoubleClickQuiz(quiz, e);
            }
        }

        public Control Control
        {
            get { return this.quizListControl; }
        }


        public void SaveQuiz(Quiz quiz)
        {
            var searchQuiz = this.quizCollection.FirstOrDefault(q => q.Id == quiz.Id);

            if (searchQuiz != null)
            {
                this.quizCollection.Remove(searchQuiz);
            }

            this.quizCollection.Add(quiz);
        }



        public void LoadQuizList(IEnumerable<Quiz> collection)
        {
            if (collection != null)
            {
                this.quizCollection = new ObservableCollection<Quiz>(collection);

                InitializeBindings();
            }

            if (LoadQuizListCompleted != null)
            {
                LoadQuizListCompleted(this.quizCollection, new LoadQuizListCompletedEventArgs(this.quizCollection == null ? false : true));
            }
        }
    }
}
