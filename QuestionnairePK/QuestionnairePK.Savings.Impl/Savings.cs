﻿using log4net;
using QuestionnairePK.Model;
using QuestionnairePK.Savings.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;
using System.Xml.Serialization;
using System.Runtime.Serialization;

namespace QuestionnairePK.Savings.Impl
{
    public class Savings: ISaveJson, ISaveXml
    {
        private readonly ILog logger;

        private readonly NamedLocker namedlocker;

        public Savings(ILog log, NamedLocker namedLocker)
        {
            this.logger = log;
            this.namedlocker = namedLocker;
        }

        bool ISaveJson.SaveQuiz(Quiz quiz, string path)
        {
           return namedlocker.RunWithLock<bool>(path, () =>
            {
                try
                {
                    using (var sw = new StreamWriter(path, false))
                    {
                        string data = JsonConvert.SerializeObject(quiz);
                        sw.WriteLine(data);
                    }
                }
                catch (Exception ex)
                {
                    logger.Warn(ex.Message);
                    return false;
                }

                return true;
            }
            );
        }

        bool ISaveJson.SaveQuizList(IEnumerable<Quiz> collection, string path)
        {
            return namedlocker.RunWithLock<bool>(path, () =>
            {
                try
                {
                    using (var sw = new StreamWriter(path, false))
                    {
                        string data = JsonConvert.SerializeObject(collection, Formatting.Indented);
                        sw.WriteLine(data);
                    }
                }
                catch (Exception ex)
                {
                    logger.Warn(ex.Message);
                    return false;
                }

                return true;
            });
        }

        bool ISaveXml.SaveQuiz(Quiz quiz, string path)
        {
            return namedlocker.RunWithLock<bool>(path, () =>
            {
                try
                {
                    using (var sw = new FileStream(path, FileMode.Create, FileAccess.Write))
                    {
                        var serializer = new DataContractSerializer(typeof(Quiz));
                        serializer.WriteObject(sw, quiz);
                    }
                }
                catch (Exception ex)
                {
                    logger.Warn(ex.Message);    
                    return false;
                }

                return true;
            });
        }
    }
}
