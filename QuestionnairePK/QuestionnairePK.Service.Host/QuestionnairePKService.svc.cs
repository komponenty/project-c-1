﻿using QuestionnairePK.Loads.Contract;
using QuestionnairePK.Model;
using QuestionnairePK.Savings.Contract;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;

namespace QuestionnairePK.Service.Host
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class QuestionnairePKService : IQuestionnaireHandling
    {

        private readonly string data;

        private readonly ISaveJson saveJson;
        private readonly ILoadsJson loadsJson;

        private ICollection<Quiz> quizCollection;

        public QuestionnairePKService(ISaveJson saveJson, ILoadsJson loadJson)
        {
            this.saveJson = saveJson;
            this.loadsJson = loadJson;

            data = Path.Combine(InitializeDirectories(), "data.data");

            InitializeCollection();
        }

        public QuestionnairePKService() { }

        private void InitializeCollection()
        {
            var dataCollection = loadsJson.LoadQuizList(data);

            if (dataCollection != null)
            {
                this.quizCollection = (ICollection<Quiz>)dataCollection;
            }
            else
            {
                this.quizCollection = new HashSet<Quiz>();
            }
        }

        public IEnumerable<Quiz> GetQuizCollection()
        {
            return this.quizCollection;
        }

        public void SaveQuiz(Quiz quiz)
        {
            if (ExistQuiz(quiz.Id))
            {
                quizCollection.Remove(quizCollection.First(q => q.Id == quiz.Id));
            }

            quizCollection.Add(quiz);

            SaveListTaskAsync();
        }

        public void RemoveQuiz(Guid quizId)
        {
            if (ExistQuiz(quizId))
            {
                quizCollection.Remove(quizCollection.First(q => q.Id == quizId));

                SaveListTaskAsync();
            }
        }

        private bool ExistQuiz(Guid id)
        {
            if (quizCollection.Any(q => q.Id == id))
            {
                return true;
            }

            return false;
        }

        private void SaveListTaskAsync()
        {
            Task saveListTask = new Task(() => saveJson.SaveQuizList(quizCollection, data));
            saveListTask.Start();
        }

        private string InitializeDirectories()
        {
            string path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "QuestionnairePK");

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            return path;
        }
    }
}
