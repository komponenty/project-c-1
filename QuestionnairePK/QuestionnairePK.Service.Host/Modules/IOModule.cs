﻿using Ninject.Modules;
using QuestionnairePK.Loads.Contract;
using QuestionnairePK.Loads.Impl;
using QuestionnairePK.Savings.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuestionnairePK.Service.Host.Modules
{
    public class IOModule : NinjectModule
    {
        public override void Load()
        {
            Bind<ISaveJson>().To<QuestionnairePK.Savings.Impl.Savings>().InSingletonScope();
            Bind<ILoadsJson>().To<Load>().InSingletonScope();
        }
    }
}