﻿using log4net;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuestionnairePK.Service.Host.Modules
{
    public class LoggingModule: NinjectModule
    {
        public override void Load()
        {
            Bind<ILog>().ToMethod(l => LogManager.GetLogger(" Host Service "));
        }
    }
}