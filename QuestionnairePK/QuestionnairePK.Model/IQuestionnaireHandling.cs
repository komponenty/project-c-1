﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace QuestionnairePK.Model
{
    [ServiceContract]
    public interface IQuestionnaireHandling
    {
        [OperationContract]
        IEnumerable<Quiz> GetQuizCollection();

        [OperationContract]
        void SaveQuiz(Quiz quiz);

        [OperationContract]
        void RemoveQuiz(Guid quizId);
    }
}
