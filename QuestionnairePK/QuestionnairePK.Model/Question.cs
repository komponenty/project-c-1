﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace QuestionnairePK.Model
{
    [DataContract]
    public class Question: BaseModel
    {
        public Question()
        {
            this.Answers = new HashSet<Answer>();
        }

        [DataMember]
        public Guid QuizId { get; set; }

        [DataMember]
        public bool Type { get; set; }

        [DataMember]
        public ICollection<Answer> Answers { get; set; }
    }
}
