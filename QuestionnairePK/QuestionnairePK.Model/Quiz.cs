﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace QuestionnairePK.Model
{
    [DataContract]
    public class Quiz: BaseModel
    {
        public Quiz()
        {
            this.Questions = new HashSet<Question>();
        }

        [DataMember]
        public DateTime CreateDate { get; set; }

        [DataMember]
        public DateTime ModyficationDate { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public ICollection<Question> Questions { get; set; }
    }
}
