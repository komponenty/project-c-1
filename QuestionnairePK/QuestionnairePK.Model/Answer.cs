﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace QuestionnairePK.Model
{
    [DataContract]
    public class Answer: BaseModel
    {
        [DataMember]
        public Guid QuestionId { get; set; }

        [DataMember]
        public bool IsCorrect { get; set; }
    }
}
