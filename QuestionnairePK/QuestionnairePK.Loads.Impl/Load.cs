﻿using QuestionnairePK.Loads.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml.Serialization;
using Newtonsoft.Json;
using log4net;
using System.Runtime.Serialization;
using QuestionnairePK.Model;

namespace QuestionnairePK.Loads.Impl
{
    public class Load : ILoadsJson, ILoadsXml
    {
        private readonly ILog logger;
        private readonly QuestionnairePK.Model.NamedLocker namedlocker;

        public Load(ILog log, QuestionnairePK.Model.NamedLocker namedLocker)
        {
            this.logger = log;
            this.namedlocker = namedLocker;
        }


        Quiz ILoadsJson.LoadQuiz(string path)
        {
            return namedlocker.RunWithLock<Quiz>(path, () =>
                {
                    Quiz loadedQuiz = null;

                    try
                    {
                        if (File.Exists(path))
                        {
                            string json = File.ReadAllText(path);

                            loadedQuiz = JsonConvert.DeserializeObject<Quiz>(json);
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Error(ex.Message);
                    }


                    return loadedQuiz;
                });
        }

        IEnumerable<Quiz> ILoadsJson.LoadQuizList(string path)
        {
            return namedlocker.RunWithLock<IEnumerable<Quiz>>(path, () =>
                {
                    IEnumerable<Quiz> loadedQuizList = null;

                    try
                    {
                        if (File.Exists(path))
                        {
                            string json = File.ReadAllText(path);

                            loadedQuizList = JsonConvert.DeserializeObject<IEnumerable<Quiz>>(json);
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Error(ex.Message);
                    }


                    return loadedQuizList;
                });
        }

        Quiz ILoadsXml.LoadQuiz(string path)
        {
            return namedlocker.RunWithLock<Quiz>(path, () =>
                {
                    Quiz loadedQuiz = null;

                    try
                    {
                        if (File.Exists(path) && Path.GetExtension(path) == ".xml")
                        {
                            using (var sr = new FileStream(path, FileMode.Open))
                            {
                                var serializer = new DataContractSerializer(typeof(Quiz));
                                loadedQuiz = serializer.ReadObject(sr) as Quiz;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Error(ex.Message);
                    }


                    return loadedQuiz;
                });
        }
    }
}
