﻿using QuestionnairePK.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace QuestionnairePK.UI.CreateQuiz.Contract
{
    public interface ICreateQuiz
    {
        Quiz GetQuiz { get; }
        Control Control { get; }

        void SetQuiz(Quiz quiz);
        void ResetControll();

        event EventHandler GetQuizEvent;
    }
}
