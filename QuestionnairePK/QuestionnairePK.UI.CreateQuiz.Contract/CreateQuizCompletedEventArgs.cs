﻿using QuestionnairePK.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace QuestionnairePK.UI.CreateQuiz.Contract
{
    public class CreateQuizCompletedEventArgs: RoutedEventArgs
    {
        public Quiz GetQuiz { get; private set; }

        public CreateQuizCompletedEventArgs(Quiz quiz)
        {
            this.GetQuiz = quiz;
        }

        public CreateQuizCompletedEventArgs(RoutedEvent routedEvent, Quiz quiz)
            : base(routedEvent)
        {
            this.GetQuiz = quiz;
        }
    }
}
