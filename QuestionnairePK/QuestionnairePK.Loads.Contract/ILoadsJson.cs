﻿using QuestionnairePK.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuestionnairePK.Loads.Contract
{
    public interface ILoadsJson
    {
        Quiz LoadQuiz(string path);
        IEnumerable<Quiz> LoadQuizList(string path);
    }
}
