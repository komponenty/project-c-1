﻿using QuestionnairePK.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuestionnairePK.Loads.Contract
{
    public interface ILoadsXml
    {
        Quiz LoadQuiz(string path);
    }
}
