﻿using QuestionnairePK.Model;
using QuestionnairePK.UI.CreateQuiz.Contract;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace QuestionnariePK.UI.CreateQuiz.Impl
{
    /// <summary>
    /// Interaction logic for CreateQuizControl.xaml
    /// </summary>
    public partial class CreateQuizControl : UserControl
    {
        private SetQuizViewModel setQuizViewModel;
        private ObservableCollection<SetQuestionViewModel> setQuestionCollection;

        public static readonly RoutedEvent ClickEvent =
    EventManager.RegisterRoutedEvent("Click", RoutingStrategy.Bubble,
    typeof(RoutedEventHandler), typeof(CreateQuizControl));

        public event RoutedEventHandler Click
        {
            add { AddHandler(ClickEvent, value); }
            remove { RemoveHandler(ClickEvent, value); }
        }

        internal QuizHelper GetQuizHelper
        {
            get { return new QuizHelper(setQuizViewModel, setQuestionCollection); }
            set
            {
                this.setQuizViewModel = value.SetQuiz;
                this.setQuestionCollection = value.SetQuestionCollection;
                InitializeBindings();
            }
        }

        public CreateQuizControl()
        {
            ResetControll();
        }

        /// <summary>
        /// Reset create quiz controll
        /// </summary>
        public void ResetControll()
        {
            this.setQuizViewModel = new SetQuizViewModel();
            this.setQuestionCollection = new ObservableCollection<SetQuestionViewModel>();

            this.setQuizViewModel.QuizId = Guid.NewGuid();
            this.setQuizViewModel.CreateDate = DateTime.Now;

            InitializeQuestionCollection();
            InitializeComponent();
            InitializeBindings();

        }

        /// <summary>
        /// Initialize bindings in control
        /// </summary>
        private void InitializeBindings()
        {
            setQuiz.DataContext = setQuizViewModel;
            setQuestion.DataContext = setQuestionCollection.First();
            questions.ItemsSource = setQuestionCollection;
        }

        /// <summary>
        /// Add new question to setQuestionCollection
        /// </summary>
        private void InitializeQuestionCollection(int id = 1)
        {
            var answerList = new ObservableCollection<Answer>();

            answerList.Add(new Answer());
            answerList.Add(new Answer());
            answerList.Add(new Answer());

            this.setQuestionCollection.Add(new SetQuestionViewModel() { Answers = answerList, Id = id });
        }

        /// <summary>
        /// Save Quiz
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void setQuiz_Click(object sender, RoutedEventArgs e)
        {
            var quizHelper = new QuizHelper(setQuizViewModel, setQuestionCollection);
            var quiz = (Quiz)quizHelper;
            RaiseEvent(new CreateQuizCompletedEventArgs(ClickEvent, quiz));
        }

        private void setQuestion_AddClick(object sender, RoutedEventArgs e)
        {
            SetQuestionViewModel question = setQuestion.DataContext as SetQuestionViewModel;
            question.Answers.Add(new Answer());
        }

        private void setQuestion_RemoveClick(object sender, RoutedEventArgs e)
        {
            SetQuestionViewModel question = setQuestion.DataContext as SetQuestionViewModel;

            if (question.Answers.Count > 3)
            {
                question.Answers.RemoveAt(question.Answers.Count - 1);
            }
        }

        private void questions_MouseUp(object sender, MouseButtonEventArgs e)
        {
            this.setQuestionView.Visibility = System.Windows.Visibility.Hidden;
            this.setQuizView.Visibility = System.Windows.Visibility.Visible;
        }

        private void TextBlock_MouseUp(object sender, MouseButtonEventArgs e)
        {
            var obj = sender as TextBlock;
            int number = Int32.Parse(obj.Text.Split(' ')[1]) - 1;

            this.setQuestion.DataContext = setQuestionCollection.ElementAt(number);

            if (this.setQuestionView.Visibility == System.Windows.Visibility.Hidden)
            {
                this.setQuizView.Visibility = System.Windows.Visibility.Hidden;
                this.setQuestionView.Visibility = System.Windows.Visibility.Visible;
            }

            e.Handled = true;
        }

        /// <summary>
        /// Remove question
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Image_MouseUp(object sender, MouseButtonEventArgs e)
        {
            var obj = sender as Image;
            int removeItem = Int32.Parse(obj.Tag.ToString()) - 1;

            this.setQuestionCollection.RemoveAt(removeItem);

            for (int i = 0; i < setQuestionCollection.Count; i++)
            {
                setQuestionCollection.ElementAt(i).Id = i + 1;
            }

            e.Handled = true;
        }

        /// <summary>
        /// Add new question
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            int id = setQuestionCollection.Count + 1;
            InitializeQuestionCollection(id);
        }
    }
}
