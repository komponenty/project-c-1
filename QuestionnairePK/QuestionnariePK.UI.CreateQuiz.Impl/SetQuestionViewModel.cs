﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuestionnariePK.UI.CreateQuiz.Impl
{
    class SetQuestionViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private ObservableCollection<Answer> answers;

        private int id;
        private string nameQuestion;
        private bool isMulti;

        public SetQuestionViewModel()
        {
            this.answers = new ObservableCollection<Answer>();
            this.isMulti = false;
        }

        public ObservableCollection<Answer> Answers
        {
            get { return this.answers; }
            set { this.answers = value; }
        }

        public int Id
        {
            get { return this.id; }
            set
            {
                this.id = value;
                RaisePropertyChanged("Id");
            }
        }

        public string NameQuestion
        {
            get { return this.nameQuestion; }
            set
            {
                this.nameQuestion = value;
                RaisePropertyChanged("NameQuestion");
            }
        }

        public bool IsMulti
        {
            get { return this.isMulti; }
            set
            {
                this.isMulti = value;
                RaisePropertyChanged("IsMulti");
            }
        }

        private void RaisePropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }

    class Answer : INotifyPropertyChanged
    {
        private bool isCorrect;
        private string name;


        public event PropertyChangedEventHandler PropertyChanged;

        public Answer()
        {
            this.isCorrect = false;
        }

        private void RaisePropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public bool IsCorrect
        {
            get { return this.isCorrect; }
            set
            {
                this.isCorrect = value;

                RaisePropertyChanged("IsCorrect");
            }
        }

        public string Name
        {
            get { return this.name; }
            set
            {
                this.name = value;
                RaisePropertyChanged("Name");
            }
        }
    }
}
