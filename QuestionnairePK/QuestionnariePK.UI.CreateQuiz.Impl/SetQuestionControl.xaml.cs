﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace QuestionnariePK.UI.CreateQuiz.Impl
{
    /// <summary>
    /// Interaction logic for SetQuestionControl.xaml
    /// </summary>
    public partial class SetQuestionControl : UserControl
    {
        public static readonly RoutedEvent AddClickEvent =
    EventManager.RegisterRoutedEvent("Click", RoutingStrategy.Bubble,
    typeof(RoutedEventHandler), typeof(SetQuestionControl));

        public static readonly RoutedEvent RemoveClickEvent =
    EventManager.RegisterRoutedEvent("Click_1", RoutingStrategy.Bubble,
    typeof(RoutedEventHandler), typeof(SetQuestionControl));

        public event RoutedEventHandler AddClick
        {
            add { AddHandler(AddClickEvent, value); }
            remove { RemoveHandler(AddClickEvent, value); }
        }

        public event RoutedEventHandler RemoveClick
        {
            add { AddHandler(RemoveClickEvent, value); }
            remove { RemoveHandler(RemoveClickEvent, value); }
        }


        public SetQuestionControl()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Add
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            RaiseEvent(new RoutedEventArgs(AddClickEvent));
        }

        /// <summary>
        /// Remove
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            RaiseEvent(new RoutedEventArgs(RemoveClickEvent));
        }
    }
}
