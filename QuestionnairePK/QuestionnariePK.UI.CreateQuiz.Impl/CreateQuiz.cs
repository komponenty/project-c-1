﻿using QuestionnairePK.Model;
using QuestionnairePK.UI.CreateQuiz.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace QuestionnariePK.UI.CreateQuiz.Impl
{
    public class CreateQuiz: ICreateQuiz
    {
        public event EventHandler GetQuizEvent;
        private CreateQuizControl createQuizControl;

        public CreateQuiz()
        {
            this.createQuizControl = new CreateQuizControl();
            this.createQuizControl.Click += createQuizControl_Click;
        }

        void createQuizControl_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            var quiz = (e as CreateQuizCompletedEventArgs).GetQuiz;

            if (GetQuizEvent != null)
            {
                GetQuizEvent(quiz, e);
            }
        }


        public Control Control
        {
            get { return this.createQuizControl; }
        }

        public Quiz GetQuiz
        {
            get 
            {
                return (Quiz)createQuizControl.GetQuizHelper;
            }
        }

        public void SetQuiz(Quiz quiz)
        {
            this.createQuizControl.GetQuizHelper = (QuizHelper)quiz;
        }



        public void ResetControll()
        {
            this.createQuizControl.ResetControll();
        }
    }
}
