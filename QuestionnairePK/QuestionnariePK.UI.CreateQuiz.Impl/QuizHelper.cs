﻿using QuestionnairePK.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuestionnariePK.UI.CreateQuiz.Impl
{
    class QuizHelper
    {
        internal SetQuizViewModel SetQuiz { get; set; }
        internal ObservableCollection<SetQuestionViewModel> SetQuestionCollection;

        public QuizHelper()
        { }

        public QuizHelper(SetQuizViewModel quiz, ObservableCollection<SetQuestionViewModel> collection)
        {
            this.SetQuiz = quiz;
            this.SetQuestionCollection = collection;
        }

        public static explicit operator Quiz(QuizHelper helper)
        {
            Quiz quiz = new Quiz();

            quiz.Id = helper.SetQuiz.QuizId;
            quiz.CreateDate = helper.SetQuiz.CreateDate;
            quiz.ModyficationDate = DateTime.Now;
            quiz.Name = helper.SetQuiz.TitleQuiz;
            quiz.Description = helper.SetQuiz.DescriptionQuiz;

            foreach (var item in helper.SetQuestionCollection)
            {
                Question q = new Question();

                q.Id = Guid.NewGuid();
                q.QuizId = quiz.Id;
                q.Name = item.NameQuestion;
                q.Type = item.IsMulti;

                foreach (var answer in item.Answers)
                {
                    QuestionnairePK.Model.Answer a = new QuestionnairePK.Model.Answer();

                    a.IsCorrect = answer.IsCorrect;
                    a.Name = answer.Name;
                    a.Id = Guid.NewGuid();
                    a.QuestionId = q.Id;
                    
                    q.Answers.Add(a);
                }

                quiz.Questions.Add(q);
            }

            return quiz;
        }

        public static explicit operator QuizHelper(Quiz quiz)
        {
            QuizHelper helper = new QuizHelper();

            helper.SetQuiz = new SetQuizViewModel();
            helper.SetQuestionCollection = new ObservableCollection<SetQuestionViewModel>();

            helper.SetQuiz.QuizId = quiz.Id;
            helper.SetQuiz.CreateDate = quiz.CreateDate;
            helper.SetQuiz.DescriptionQuiz = quiz.Description;
            helper.SetQuiz.TitleQuiz = quiz.Name;

            int counter = 1;
            foreach (var item in quiz.Questions)
            {

                SetQuestionViewModel sq = new SetQuestionViewModel();

                sq.Id = counter;
                sq.IsMulti = item.Type;
                sq.NameQuestion = item.Name;

                foreach (var a in item.Answers)
                {
                    Answer answer = new Answer();

                    answer.Name = a.Name;
                    answer.IsCorrect = a.IsCorrect;
                    sq.Answers.Add(answer);
                }

                helper.SetQuestionCollection.Add(sq);
                counter++;
            }

            return helper;
        }
    }
}
