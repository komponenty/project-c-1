﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuestionnariePK.UI.CreateQuiz.Impl
{
    class SetQuizViewModel : INotifyPropertyChanged 
    {
        private string titleQuiz;
        private string descriptionQuiz;

        public Guid QuizId { get; set; }
        public DateTime CreateDate { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        private void RaisePropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public string TitleQuiz
        {
            get { return this.titleQuiz; }
            set
            {
                titleQuiz = value;
                RaisePropertyChanged("TitleQuiz");
            }
        }

        public string DescriptionQuiz
        {
            get { return this.descriptionQuiz; }
            set
            {
                descriptionQuiz = value;
                RaisePropertyChanged("DescriptionQuiz");
            }
        }
    }
}
