﻿using QuestionnairePK.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuestionnairePK.Savings.Contract
{
    public interface ISaveJson
    {
        bool SaveQuiz(Quiz quiz, string path);
        bool SaveQuizList(IEnumerable<Quiz> collection, string path);
    }
}
