﻿using log4net;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuestionnairePK.Infrastructure.Modules
{
    class LoggingModule: NinjectModule
    {
        public override void Load()
        {
            Bind<ILog>().ToMethod(
                l => LogManager.GetLogger(
                    l.Request.ParentRequest == null ? l.Request.Service.FullName : l.Request.ParentRequest.Service.FullName
                    ));
        }
    }
}
