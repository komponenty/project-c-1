﻿using Ninject.Modules;
using QuestionnairePK.Loads.Contract;
using QuestionnairePK.Loads.Impl;
using QuestionnairePK.Savings.Contract;
using QuestionnairePK.Savings.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuestionnairePK.Infrastructure.Modules
{
    class IOModule: NinjectModule
    {
        public override void Load()
        {
            Bind<ILoadsJson>().To<Load>();
            Bind<ILoadsXml>().To<Load>();

            Bind<ISaveJson>().To<Savings.Impl.Savings>();
            Bind<ISaveXml>().To<Savings.Impl.Savings>();
        }
    }
}
