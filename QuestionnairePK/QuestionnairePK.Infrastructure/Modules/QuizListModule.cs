﻿using Ninject.Modules;
using QuestionnairePK.UI.QuizList.Contract;
using QuestionnairePK.UI.QuizList.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuestionnairePK.Infrastructure.Modules
{
    class QuizListModule: NinjectModule
    {
        public override void Load()
        {
            Bind<IQuizList>().To<QuizList>().InSingletonScope();
        }
    }
}
