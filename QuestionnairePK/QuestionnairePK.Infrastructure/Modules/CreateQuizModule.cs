﻿using Ninject.Modules;
using QuestionnairePK.UI.CreateQuiz.Contract;
using QuestionnariePK.UI.CreateQuiz.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuestionnairePK.Infrastructure.Modules
{
    class CreateQuizModule: NinjectModule
    {
        public override void Load()
        {
            Bind<ICreateQuiz>().To<CreateQuiz>().InSingletonScope();
        }
    }
}
