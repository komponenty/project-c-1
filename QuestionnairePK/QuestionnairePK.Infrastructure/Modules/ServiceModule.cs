﻿using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Ninject.Extensions.Wcf.Client;
using QuestionnairePK.Infrastructure.ServiceReference;

namespace QuestionnairePK.Infrastructure.Modules
{
    class ServiceModule: NinjectModule
    {
        public override void Load()
        {
            Bind<IQuestionnaireHandling>().To<QuestionnaireHandlingClient>().InSingletonScope();
        }
    }
}
