﻿using Ninject.Modules;
using QuestionnairePK.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuestionnairePK.Infrastructure.Modules
{
    class LockerModule: NinjectModule
    {
        public override void Load()
        {
            Bind<NamedLocker>().ToSelf().InSingletonScope();
        }
    }
}
