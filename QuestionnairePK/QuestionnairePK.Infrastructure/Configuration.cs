﻿using Ninject;
using QuestionnairePK.Infrastructure.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuestionnairePK.Infrastructure
{
    public class Configuration
    {
        private static IKernel container;

        public static IKernel Container
        {
            get
            {
                if (container == null)
                    container = ConfigureApp();
                return container;
            }
        }

        private static IKernel ConfigureApp()
        {
            var kernel = new StandardKernel(
                new QuizListModule(),
                new CreateQuizModule(),
                new LoggingModule(),
                new IOModule(),
                new LockerModule(),
                new ServiceModule()
            );

            return kernel;
        }
    }
}
