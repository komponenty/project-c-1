﻿using QuestionnairePK.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace QuestionnairePK.UI.QuizList.Contract
{
    public interface IQuizList
    {
        event EventHandler LoadQuizListCompleted;
        event EventHandler DoubleClickQuiz;
        event EventHandler RemoveQuizComplete;

        void LoadQuizList(IEnumerable<Quiz> collection);
        void SaveQuiz(Quiz quiz);


        Control Control { get; }
    }
}
