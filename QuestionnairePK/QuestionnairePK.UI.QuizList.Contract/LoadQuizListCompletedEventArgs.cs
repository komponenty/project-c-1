﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuestionnairePK.UI.QuizList.Contract
{
    public class LoadQuizListCompletedEventArgs: EventArgs
    {
        public bool IsCompleted { get; private set; }

        public LoadQuizListCompletedEventArgs(bool completed)
        {
            this.IsCompleted = completed;
        }
    }
}
