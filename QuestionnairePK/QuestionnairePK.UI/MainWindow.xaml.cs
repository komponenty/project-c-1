﻿using log4net;
using log4net.Config;
using Microsoft.Win32;
using QuestionnairePK.Loads.Contract;
using QuestionnairePK.Model;
using QuestionnairePK.Savings.Contract;
using QuestionnairePK.UI.CreateQuiz.Contract;
using QuestionnairePK.UI.QuizList.Contract;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Service = QuestionnairePK.Infrastructure.ServiceReference;

namespace QuestionnairePK.UI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly ICreateQuiz createQuiz;
        private readonly IQuizList quizList;
        private readonly ILoadsJson loadJson;
        private readonly ILoadsXml loadXml;
        private readonly ISaveJson saveJson;
        private readonly ISaveXml saveXml;
        private readonly ILog logger;
        private readonly Service.IQuestionnaireHandling questionnaireHandling;


        public MainWindow(ICreateQuiz createQuiz, IQuizList quizList, ILoadsJson loadJson, ILoadsXml loadXml, ISaveJson saveJson, ISaveXml saveXml, ILog logger, Service.IQuestionnaireHandling service)
        {
            this.createQuiz = createQuiz;
            this.quizList = quizList;
            this.loadJson = loadJson;
            this.loadXml = loadXml;
            this.saveJson = saveJson;
            this.saveXml = saveXml;
            this.logger = logger;
            this.questionnaireHandling = service;


            XmlConfigurator.Configure();

            InitializeComponent();
        }

        #region Events

        private void Window_Initialized(object sender, EventArgs e)
        {
            this.quizListContent.Content = quizList.Control;
            this.createQuizContent.Content = createQuiz.Control;


            var collection = questionnaireHandling.GetQuizCollection();

            this.quizList.LoadQuizList(collection);

            InitializedEvents();
        }

        /// <summary>
        /// Initialize event controll component
        /// </summary>
        private void InitializedEvents()
        {
            this.createQuiz.GetQuizEvent += createQuiz_GetQuizEvent;
            this.quizList.DoubleClickQuiz += quizList_DoubleClickQuiz;
            this.quizList.RemoveQuizComplete += quizList_RemoveQuizComplete;
        }

        /// <summary>
        /// Inform service that quiz was removed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void quizList_RemoveQuizComplete(object sender, EventArgs e)
        {
            var quiz = sender as Quiz;

            if (quiz != null)
            {
                questionnaireHandling.RemoveQuiz(quiz.Id);
            }
        }

        /// <summary>
        /// Double click on quiz in quiz list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void quizList_DoubleClickQuiz(object sender, EventArgs e)
        {
            try
            {
                var item = sender as Quiz;

                createQuiz.SetQuiz(item);

                CreateQuiz_Click(null, null);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);

                MessageBox.Show("Nie można było wczytać quizu", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// Save quiz from create quiz control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void createQuiz_GetQuizEvent(object sender, EventArgs e)
        {
            var item = sender as Quiz;

            SaveQuiz(item);
        }

        /// <summary>
        /// Create new quiz event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CreateQuiz_Click(object sender, RoutedEventArgs e)
        {
            this.quizListContent.Visibility = System.Windows.Visibility.Hidden;
            this.createQuizContent.Visibility = System.Windows.Visibility.Visible;
        }

        /// <summary>
        /// Import quiz from json format event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ImportFromJson_Click(object sender, RoutedEventArgs e)
        {
            var fileDialog = new OpenFileDialog();
            fileDialog.Filter = "Json Files | *.json";
            fileDialog.DefaultExt = "json";

            var result = fileDialog.ShowDialog();

            if (result.HasValue && result.Value)
            {
                try
                {
                    string path = fileDialog.FileName;
                    var quiz = loadJson.LoadQuiz(path);

                    if (quiz == null)
                    {
                        throw new Exception("Wrong format file");
                    }

                    quizList.SaveQuiz(quiz);

                    ShowListQuiz_Click(null, null);
                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message);

                    MessageBox.Show("Nie można było wczytać quizu.", "Błąd wczytania", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        /// <summary>
        /// Import quiz from xml event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ImportFromXml_Click(object sender, RoutedEventArgs e)
        {
            var fileDialog = new OpenFileDialog();
            fileDialog.Filter = "Xml Files | *.xml";
            fileDialog.DefaultExt = "xml";

            var result = fileDialog.ShowDialog();

            if (result.HasValue && result.Value)
            {
                try
                {
                    string path = fileDialog.FileName;
                    var quiz = loadXml.LoadQuiz(path);

                    if (quiz == null)
                    {
                        throw new Exception("Wrong format file");
                    }

                    quizList.SaveQuiz(quiz);

                    ShowListQuiz_Click(null, null);
                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message);

                    MessageBox.Show("Nie można było wczytać quizu.", "Błąd wczytania", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        /// <summary>
        /// Eksport to json file event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EksportToJson_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog fileDialog = new SaveFileDialog();
            fileDialog.Filter = "Json Files | *.json";
            fileDialog.DefaultExt = "json";

            var result = fileDialog.ShowDialog();

            if (result.HasValue && result.Value)
            {
                string path = fileDialog.FileName;
                var quiz = createQuiz.GetQuiz;

                saveJson.SaveQuiz(quiz, path);

                createQuiz.ResetControll();
            }
        }

        /// <summary>
        /// Eksport to xml file event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EksportToXml_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog fileDialog = new SaveFileDialog();
            fileDialog.Filter = "Xml Files | *.xml";
            fileDialog.DefaultExt = "xml";

            var result = fileDialog.ShowDialog();

            if (result.HasValue && result.Value)
            {
                string path = fileDialog.FileName;
                var quiz = createQuiz.GetQuiz;

                bool complete = saveXml.SaveQuiz(quiz, path);

                if (complete)
                {
                    createQuiz.ResetControll();
                }
                else
                {
                    MessageBox.Show("Nie udało się wyeksportować quizu", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
        }

        /// <summary>
        /// Get Authors event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GetAuthors_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Autorzy: Mateusz Mazurek, Oskar Rosołowski, Maciej Jaksa", "Developerzy oprogramowania");
        }

        /// <summary>
        /// Get program about event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AboutProgram_Click(object sender, RoutedEventArgs e)
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
            string version = fvi.FileVersion;

            MessageBox.Show(String.Format("Aplikacja do tworzenia ankiet. Version {0}", version), "Aplikacja ankieterska");
        }


        /// <summary>
        /// Show quiz list event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ShowListQuiz_Click(object sender, RoutedEventArgs e)
        {
            this.quizListContent.Visibility = System.Windows.Visibility.Visible;
            this.createQuizContent.Visibility = System.Windows.Visibility.Hidden;
        }

        /// <summary>
        /// Save quiz from menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SaveQuiz_Click(object sender, RoutedEventArgs e)
        {
            var quiz = createQuiz.GetQuiz;

            SaveQuiz(quiz);
        }

        #endregion

        /// <summary>
        /// Private method to save quiz
        /// </summary>
        /// <param name="quiz"></param>
        private void SaveQuiz(Quiz quiz)
        {
            if (quiz != null)
            {
                if (String.IsNullOrEmpty(quiz.Name) || quiz.Name.Length < 1 || quiz.Questions.Count < 2)
                {
                    MessageBox.Show("Nie masz dodanego tytułu lub/i pytań do ankiety. Aby je dodać kliknij Quiz -> Nowy");
                    return;
                }

                questionnaireHandling.SaveQuiz(quiz);
                quizList.SaveQuiz(quiz);

                createQuiz.ResetControll();

                ShowListQuiz_Click(null, null);
            }
        }

    }
}
