﻿using QuestionnairePK.Infrastructure;
using QuestionnairePK.UI.QuizList.Contract;
using Ninject;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using QuestionnairePK.UI.CreateQuiz.Contract;
using QuestionnairePK.Loads.Contract;
using QuestionnairePK.Savings.Contract;
using log4net;
using System.IO;
using QuestionnairePK.Infrastructure.ServiceReference;

namespace QuestionnairePK.UI
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            var container = Configuration.Container;

            var quizList = container.Get<IQuizList>();
            var createQuiz = container.Get<ICreateQuiz>();
            var loadJson = container.Get<ILoadsJson>();
            var loadXml = container.Get<ILoadsXml>();
            var saveJson = container.Get<ISaveJson>();
            var saveXml = container.Get<ISaveXml>();
            var logger = container.Get<ILog>();
            var service = container.Get<IQuestionnaireHandling>();

            var mainWindow = new MainWindow(createQuiz, quizList, loadJson, loadXml, saveJson, saveXml, logger, service);
            mainWindow.Show();
        }
    }
}
